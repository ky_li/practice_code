#include<stdio.h>
#define N 100010
typedef long long LL;
int n, q[N], a[N];

LL cutArr() {
    int s = 0;
    for (int i = 1; i <= n; i ++) s += q[i];
    if (s % 3 != 0 || n < 3) return 0;
    LL cnt = 0, res = 0;
    for (int i = 1; i < n; i ++) {
        if (i > 1 && a[i] == s / 3 * 2) res += cnt;
        if (a[i] == s / 3) cnt ++;
    }
    return res;
}

int main() {
    scanf("%d", &n);
    for (int i = 1; i <= n; i ++) {
        scanf("%d", &q[i]);
        a[i] = a[i - 1] + q[i];
    }
    printf("%lld", cutArr());
    return 0;
}
