#include<stdio.h>
#define N 100010
int q[N], a[N];
int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; i ++) scanf("%d", &q[i]), a[i] = a[i - 1] + q[i];
    while (m --) {
        int l, r;
        scanf("%d%d", &l, &r);
        printf("%d\n", a[r] - a[l - 1]);
    }
    return 0;
}
